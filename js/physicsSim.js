window.addEventListener('load',eventWindowLoaded,false);

function eventWindowLoaded() { canvasApp(); }

function canvasSupport () { return Modernizr.canvas; }

function canvasApp() {

    if(!canvasSupport()) { return; }

    // Define: DOM Window Dimensions
    var domWindow = {
        width: window.innerWidth - 16,
        height: window.innerHeight - 4,
        xCenter: (window.innerWidth / 2),
        yCenter: (window.innerHeight / 2),
        drawBackdrop: function() {
            context.rect(0,0,domWindow.width,domWindow.height);
            context.fillStyle = 'white';
            context.fill();
        }
    }

    // Define variables
    var numBalls = 200;
    var maxSize = 15;
    var minSize = 1;
    var maxSpeed = maxSize+5;
    var friction = .001;
    var balls = new Array();
    var tempBall;
    var tempX;
    var tempY;
    var tempSpeed;
    var tempAngle;
    var tempRadius;
    var tempRadians;
    var tempVelocityX;
    var tempVelocityY;

    function drawScreen() {

        // Set: canvas
        context.fillStyle = '#67B3C5';
        context.fillRect(0,0,theCanvas.width,theCanvas.height);

        // Draw: rectangle
        context.strokeStyle = '#90003C';
        context.strokeRect(1,1,theCanvas.width-2,theCanvas.height-2);

        // Methods: to handle collision detection(s), & finally iterating through animation
        update();
        testWalls();
        collide();
        render();
    }

    function update() {

        for (var i = 0; i < balls.length; i++) {

            ball = balls[i];

            // Friction
            ball.velocityX = ball.velocityX - (ball.velocityX*friction);
            ball.velocityY = ball.velocityY - (ball.velocityY*friction);

            ball.nextX = (ball.x += ball.velocityX);
            ball.nextY = (ball.y += ball.velocityY);
        }
    }

    function testWalls() {

        var ball;
        var testBall;

        for (var i = 0; i < balls.length; i++) {

            ball = balls[i];

            if (ball.nextX + ball.radius > theCanvas.width) {

                ball.velocityX = ball.velocityX*-1;
                ball.nextX = theCanvas.width - ball.radius;

            } else if (ball.nextX - ball.radius < 0) {

                ball.velocityX = ball.velocityX*-1;
                ball.nextX = ball.radius;

            } else if (ball.nextY + ball.radius > theCanvas.height) {

                ball.velocityY = ball.velocityY*-1;
                ball.nextY = theCanvas.height - ball.radius;

            } else if (ball.nextY - ball.radius < 0) {

                ball.velocityY = ball.velocityY*-1;
                ball.nextY = ball.radius;

            }
        }
    }

    function render() {

        var ball;

        context.fillStyle = "#90003C";

        for (var i = 0; i < balls.length; i++) {

            ball = balls[i];
            ball.x = ball.nextX;
            ball.y = ball.nextY;

            context.beginPath();
            context.arc(ball.x,ball.y,ball.radius,0,Math.PI*2,true);
            context.closePath();
            context.fill();
        }
    }

    function collide() {

        var ball;
        var testBall;

        for (var i = 0; i < balls.length; i++) {

            ball = balls[i];

            for (var j = i+ 1; j < balls.length; j++) {

                testBall = balls[j];

                if (hitTestCircle(ball,testBall)) {
                    collideBalls(ball,testBall);
                }
            }
        }
    }

    function hitTestCircle(ball1,ball2) {

        var retVal = false;

        var dx = ball1.nextX - ball2.nextX;
        var dy = ball1.nextY - ball2.nextY;

        var distance = (dx * dx + dy * dy);

        if (distance <= (ball1.radius + ball2.radius) * (ball1.radius + ball2.radius)) {
            retVal = true;
        }

        return retVal;
    }

    function collideBalls(ball1,ball2) {

        // Calculate the difference between the center pionts of each object
        var dx = ball1.nextX - ball2.nextX;
        var dy = ball1.nextY - ball2.nextY;

        // Get angle in radians, between two objects
        var collisionAngle = Math.atan2(dy,dx);

        var speed1 = Math.sqrt(ball1.velocityX * ball1.velocityX + ball1.velocityY * ball1.velocityY);
        var speed2 = Math.sqrt(ball2.velocityX * ball2.velocityX + ball2.velocityY * ball2.velocityY);

        // Calculate the Velocity Vector
        var direction1 = Math.atan2(ball1.velocityY,ball1.velocityX);
        var direction2 = Math.atan2(ball2.velocityY,ball2.velocityX);

        // Taking the angle of the collision and making it flat so we can bounce objects
        var velocityX_1 = speed1 * Math.cos(direction1 - collisionAngle);
        var velocityY_1 = speed1 * Math.sin(direction1 - collisionAngle);
        var velocityX_2 = speed2 * Math.cos(direction2 - collisionAngle);
        var velocityY_2 = speed2 * Math.sin(direction2 - collisionAngle);

        // Only x velocity needs to be updated, y stays constant
        var final_velocityX_1 = ((ball1.mass - ball2.mass) * velocityX_1 + (ball2.mass + ball2.mass) * velocityX_2) / (ball1.mass + ball2.mass);
        var final_velocityX_2 = ((ball1.mass + ball1.mass) * velocityX_1 + (ball2.mass - ball1.mass) * velocityX_2) / (ball1.mass + ball2.mass);

        var final_velocityY_1 = velocityY_1;
        var final_velocityY_2 = velocityY_2;

        // Rotate angles back again to preserve the collision angle
        ball1.velocityX = Math.cos(collisionAngle) * final_velocityX_1 + Math.cos(collisionAngle + Math.PI/2) * final_velocityY_1;
        ball1.velocityY = Math.sin(collisionAngle) * final_velocityX_1 + Math.sin(collisionAngle + Math.PI/2) * final_velocityY_1;
        ball2.velocityX = Math.cos(collisionAngle) * final_velocityX_2 + Math.cos(collisionAngle + Math.PI/2) * final_velocityY_2;
        ball2.velocityY = Math.sin(collisionAngle) * final_velocityX_2 + Math.sin(collisionAngle + Math.PI/2) * final_velocityY_2;

        // Update nextX & nextY for both objects, & store them for another collision
        ball1.nextX = (ball1.nextX += ball1.velocityX);
        ball1.nextY = (ball1.nextY += ball1.velocityY);
        ball2.nextX = (ball2.nextX += ball2.velocityX);
        ball2.nextY = (ball2.nextY += ball2.velocityY);

    }

    // Get theCanvas, and Get context
    theCanvas = document.getElementById('cvs');
    context = theCanvas.getContext('2d');

    // Define: DOM Window Dimensions
    var domWindow = {
        width: window.innerWidth - 18,
        height: window.innerHeight - 4,
        xCenter: (window.innerWidth / 2),
        yCenter: (window.innerHeight / 2),
        drawBackdrop: function() {
            context.rect(0,0,domWindow.width,domWindow.height);
            context.fillStyle = 'white';
            context.fill();
        }
    }

    // Set: Canvas Dimensions
    theCanvas.width = domWindow.width;
    theCanvas.height = domWindow.height;

    for (var i = 0; i < numBalls; i++) {

        tempRadius = Math.floor(Math.random()*maxSize)+minSize;
        var placeOK = false;

        while(!placeOK) {

            tempX = tempRadius*3 + (Math.floor(Math.random()*theCanvas.width)-tempRadius*3);
            tempY = tempRadius*3 + (Math.floor(Math.random()*theCanvas.height)-tempRadius*3);
            tempSpeed = 4;
            tempAngle = Math.floor(Math.random()*360);
            tempRadians = tempAngle * Math.PI/180;
            tempVelocityX = Math.cos(tempRadians) * tempSpeed;
            tempVelocityY = Math.sin(tempRadians) * tempSpeed;

            tempBall = {x:tempX,y:tempY,nextX:tempX,nextY:tempY,radius:tempRadius,speed:tempSpeed,angle:tempAngle,velocityX:tempVelocityX,velocityY:tempVelocityY,mass:tempRadius};

            placeOK = canStartHere(tempBall);
        }

        balls.push(tempBall);
    }

    function canStartHere(ball) {

        var retVal = true;

        for (var i = 0; i < balls.length; i++) {

            if (hitTestCircle(ball,balls[i])) {
                retVal = false;
            }
        }

        return retVal;
    }

    setInterval(drawScreen,33);

}