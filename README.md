<h1>Physics Simulation</h1>

<p>
This basic physics simulator generates 200 dynamically interacting balls, complete with
collision detection, synthesized atmospheric friction, and animation. Each ball is instantiated
through an individual element within an array, where they are then provided with their own unique
properties (e.g., size, velocity, starting coordinates, angle of trajectory, etc…). Although this
little application is simply an interesting animation at the moment, a future iteration will be
available soon for download (on GitHub), offering a user interface, allowing users to interact with
the various properties held inside the simulator.
</p>

<h1>Live Demo</h1>

<p>
<a href="http://byrne-systems.com/physics-simulation/">http://byrne-systems.com/physics-simulation/</a>
</p>
